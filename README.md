<!-- ABOUT THE PROJECT -->

- [AISS CV Lego](#aiss-cv-lego)
  - [First steps](#first-steps)
  - [Prerequisites](#prerequisites)
  - [Cloning the repository](#cloning-the-repository)
  - [Dependencies](#dependencies)
    - [Setting up (building the images)](#setting-up-building-the-images)
    - [Development (running the container)](#development-running-the-container)
  - [Common issues and fixes](#common-issues-and-fixes)
    - [Error loading docker meta data](#error-loading-docker-meta-data)

# AISS CV Lego

This repository serves as easy entry to get started with a dockerized jupyter notebook. It is meant to avoid issues regarding version control, dependencies and packages as well as issues like the "running on my machine" problem.

You may use this as a template. This repository is not meant to be updated with project-specific code but rather serve as a starting point for your task.

## First steps

## Prerequisites

- Visual Studio Code (optional, you can use any editor of your choice) – [https://code.visualstudio.com/](https://code.visualstudio.com/)
- [Docker](https://docs.docker.com/get-docker/)
  - Additionally on Windows:
    Install [Windows Subsystem for Linux (WSL2)](https://docs.microsoft.com/en-us/windows/wsl/install). See [this](https://ubuntu.com/tutorials/install-ubuntu-on-wsl2-on-windows-10#1-overview) concrete example for Ubuntu.
- [Docker Compose](https://docs.docker.com/compose/install/)

## Cloning the repository

1. Clone the repo from [https://git.scc.kit.edu/aiss_cv/docker-development-environment.git](https://git.scc.kit.edu/aiss_cv/docker-development-environment.git)
   ```bash
      git clone https://git.scc.kit.edu/aiss_cv/docker-development-environment.git
   ```
2. Change to the `docker-development-environment/` directory.
   ```bash
   cd docker-development-environment/
   ```

## Dependencies

This dockerized version contains one service:

- `jupyter`

You can extend it to your liking by adding them to the `packages/environment_x.yml` file(s) and rebuilding the container.

### Setting up (building the images)

After cloning the repository and changing to the `docker-development-environment/` directory (see [First steps](#first-steps)), follow the steps below to setup up the project.

1.  Build the Docker container:

    ```bash
    docker-compose build

    # ... wait for the build to finish ...

    ```

### Development (running the container)

1. Run the Docker container:

   ```bash
   # -d runs the containers in the background
   docker-compose up

   # ... develop and interact with container ...

   ```

   You can now interact with the container. The `jupyter notebook` container is running on [http://localhost:8888](http://localhost:8888).

2. Stop the container:

   ```bash
   # -v removes the volumes
   docker-compose down
   ```

   Consider using the `-v` flag to remove the volumes.

## Common issues and fixes

### Error loading docker meta data

(Encountered on Mac OS on Intel & Apple Silicon)

```console
=> ERROR [internal] load metadata for docker.io/continuumio/miniconda3:23.3.1-0
```

Fix:

```console
$ rm ~/.docker/config.json
```
