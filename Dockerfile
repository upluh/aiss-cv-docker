FROM continuumio/miniconda3:23.3.1-0
# ? also see: https://towardsdatascience.com/making-docker-and-conda-play-well-together-eda0ff995e3c

WORKDIR /opt/notebook

# installing deps, multiple kernels supported
COPY packages packages

RUN conda env update -f packages/environment_one.yml
# use base for first
RUN jupyter kernelspec remove -f python3
RUN python -m ipykernel install --name kernel_one --display-name "Docker: Kernel One"

RUN conda env create -f packages/environment_two.yml
# switch to new env for 2nd
SHELL ["conda","run","-n","aiss_cv_two","/bin/bash","-c"]
RUN python -m ipykernel install --name kernel_two --display-name "Docker: Kernel Two"

# copying all notebooks over
COPY . .

EXPOSE 8888

CMD ["jupyter", "notebook", "--ip=*","--no-browser","--allow-root", "--notebook-dir=/opt/notebook"]